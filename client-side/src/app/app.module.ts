import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MaterialComponentsModule } from "./common/material-components.module";
import { ProjectModule } from "./modules/project/project.module";
import { TaskModule } from "./modules/task/task.module";
import { TeamModule } from "./modules/team/team.module";
import { UserModule } from "./modules/user/user.module";

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule,
    MaterialComponentsModule,
    ProjectModule,
    TaskModule,
    TeamModule,
    UserModule,
  ],
  exports: [MaterialComponentsModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
