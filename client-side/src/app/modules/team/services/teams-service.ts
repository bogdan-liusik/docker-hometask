import {Injectable} from "@angular/core";
import {HttpInternalService} from "../../../services/http-internal.service";
import {Observable} from "rxjs";
import {HttpResponse} from "@angular/common/http";
import {UpdateTeam} from "../models/update-team";
import {NewTeam} from "../models/new-team";
import {Team} from "../models/team";

@Injectable({ providedIn: 'root' })
export class TeamsService {
  public readonly routePrefix = '/api/teams';

  constructor(private httpService: HttpInternalService) { }

  public getAllTeams(): Observable<Team[]> {
    return this.httpService.getRequest<Team[]>(`${this.routePrefix}`);
  }

  public createTeam(newTeam: NewTeam): Observable<HttpResponse<Team>> {
    return this.httpService.postFullRequest<Team>(`${this.routePrefix}`, newTeam);
  }

  public updateTeam(updateTeam: UpdateTeam): Observable<HttpResponse<Team>> {
    return this.httpService.putFullRequest<Team>(`${this.routePrefix}`, updateTeam);
  }

  public deleteTeam(id: number) {
    return this.httpService.deleteFullRequest(`${this.routePrefix}/${id}`);
  }
}
