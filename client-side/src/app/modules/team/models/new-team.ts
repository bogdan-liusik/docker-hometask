export interface NewTeam {
  name: string;
  createdAt: Date;
}
