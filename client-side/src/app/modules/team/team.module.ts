import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamsComponent } from './components/teams/teams.component';
import { TeamComponent } from './components/team/team.component';
import { UpdateTeamComponent } from './components/update-team/update-team.component';
import { CreateTeamComponent } from './components/create-team/create-team.component';
import {MaterialComponentsModule} from "../../common/material-components.module";
import {ReactiveFormsModule} from "@angular/forms";
import {ProjectModule} from "../project/project.module";



@NgModule({
  declarations: [
    TeamsComponent,
    TeamComponent,
    UpdateTeamComponent,
    CreateTeamComponent
  ],
    imports: [
        CommonModule,
        MaterialComponentsModule,
        ReactiveFormsModule,
        ProjectModule
    ]
})
export class TeamModule { }
