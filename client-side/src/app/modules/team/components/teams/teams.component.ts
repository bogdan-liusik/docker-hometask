import {Component, OnInit, ViewChildren} from '@angular/core';
import {Subject} from "rxjs";
import {TeamComponent} from "../team/team.component";
import {SnackBarService} from "../../../../services/snack-bar.service";
import {TeamsService} from "../../services/teams-service";
import {map, takeUntil} from "rxjs/operators";
import {Team} from "../../models/team";

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.scss']
})
export class TeamsComponent implements OnInit {
  public teams: Team[] = [];
  public isLoading: boolean = false;
  public creatingNewTeam: boolean = false;

  private unsubscribe$ = new Subject<void>();

  @ViewChildren(TeamComponent) public innerComponents: TeamComponent[] = [];

  constructor(private teamService: TeamsService,
              private snackBarService: SnackBarService) { }

  ngOnInit(): void {
    this.loadTeams();
  }

  public addNewTeam(team: Team): void {
    this.teams.unshift(team);
    this.creatingNewTeam = false;
  }

  public deleteTeam(id: number): void {
    this.isLoading = true;

    this.teamService
      .deleteTeam(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        response => {
          this.isLoading = false;
          this.teams = this.teams.filter(team => team.id !== id);
          this.snackBarService.showSuccessMessage('Team deleted!');
        },
        error => {
          this.isLoading = false;
          this.snackBarService.showErrorMessage(error);
        }
      );
  }

  private loadTeams(): void {
    this.isLoading = true;

    this.teamService
      .getAllTeams()
      .pipe(
        takeUntil(this.unsubscribe$),
        map(projects => projects.sort((project1, project2) => project2.id - project1.id))
      )
      .subscribe(teams => {
        this.teams = teams;
        this.isLoading = false;
      }, error => {
        this.isLoading = false;
        this.snackBarService.showErrorMessage('cant load data from server :(')
      });
  }
}
