import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {DeleteWarningDialogComponent} from "../../../../common/delete-warning-dialog/delete-warning-dialog.component";
import {Team} from "../../models/team";

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss']
})
export class TeamComponent implements OnInit {
  @Input() public team: Team = {} as Team;
  @Output() public onTeamDelete = new EventEmitter<number>();

  public isUpdating: boolean = false;

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void { }

  updateTeam(team: Team): void{
    this.isUpdating = false;
    this.team = team;
  }

  deleteTeam(): void{
    const dialogRef = this.dialog.open(DeleteWarningDialogComponent, {
      width: '400px'
    });

    dialogRef
      .afterClosed()
      .subscribe(result => {
        if(result == true) this.onTeamDelete.emit(this.team.id);
      });
  }
}
