export interface NewTask{
  name: string;
  description: string;
  projectId: number;
  performerId: number;
  state: TaskState;
  createdAt: Date;
}
