import {TaskState} from "./enums/task-state";

export interface Task{
  id: number;
  name: string;
  description: string;
  state: TaskState,
  projectId: number,
  performerId: number;
  createdAt: Date;
  finishedAt: Date;
}
