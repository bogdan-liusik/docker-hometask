export interface UpdateTask{
  id: number;
  name: string;
  description: string;
  state: TaskState;
}
