import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from "rxjs";
import {TasksComponent} from "../components/tasks/tasks.component";

@Injectable({
  providedIn: 'root'
})
export class TasksGuard implements CanDeactivate<TasksComponent>{
  canDeactivate(
    component: TasksComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot)
    : Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const updatingTask: boolean =  component.innerComponents
      .some(component => component.isUpdating);

    if(component.creatingNewTask || updatingTask){
      return window.confirm('You have unsaved changes! Leave?')
    }

    return true;
  }
}
