import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Subject} from "rxjs";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Task} from "../../models/task";
import {SnackBarService} from "../../../../services/snack-bar.service";
import {TasksService} from "../../services/tasks-service";
import {takeUntil} from "rxjs/operators";
import {UpdateTask} from "../../models/update-task";
import {TaskState} from "../../models/enums/task-state";
import {taskConstants} from "../../models/taskConstants";

@Component({
  selector: 'app-update-task',
  templateUrl: './update-task.component.html',
  styleUrls: ['./update-task.component.scss']
})
export class UpdateTaskComponent implements OnInit {
  @Input() public task: Task = {} as Task;
  @Output() onCancelUpdate = new EventEmitter<void>();
  @Output() onTaskUpdated = new EventEmitter<Task>();

  public isLoading: boolean = false;
  private unsubscribe$ = new Subject<void>();
  public formGroup: FormGroup = {} as FormGroup;

  public constants = taskConstants;

  enumKeys: number[] = [0, 1, 2, 3];
  states = TaskState;

  constructor(private taskService: TasksService,
              private snackBarService: SnackBarService) { }

  ngOnInit(): void {
    this.formGroup = new FormGroup({
      id: new FormControl(
        this.task.id
      ),

      name: new FormControl(
        this.task.name,
        [
          Validators.required,
          Validators.minLength(this.constants.MIN_TASK_NAME_LENGTH) ,
          Validators.maxLength(this.constants.MAX_TASK_NAME_LENGTH)]
      ),

      description: new FormControl(
        this.task.description,
        [
          Validators.required,
          Validators.minLength(this.constants.MIN_TASK_DESCRIPTION_LENGTH),
          Validators.maxLength(this.constants.MAX_TASK_DESCRIPTION_LENGTH)]
      ),

      state: new FormControl(
        this.task.state,
        [
          Validators.min(0),
          Validators.max(3)]
      ),
    });
  }

  onSubmit(): void {
    this.isLoading = true;
    const updateTask: UpdateTask = <UpdateTask>this.formGroup.value;

    this.taskService
      .updateTask(updateTask)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        response => {
          this.isLoading = false;
          this.task = <Task>response.body;
          this.onTaskUpdated.emit(this.task);
          this.snackBarService.showSuccessMessage('Successfully updated!');
        },
        error => {
          console.log(error)
          this.isLoading = false;
          this.snackBarService.showErrorMessage(error.error.error);
        }
      )
  }
}
