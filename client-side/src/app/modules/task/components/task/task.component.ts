import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Task} from "../../models/task";
import {DeleteWarningDialogComponent} from "../../../../common/delete-warning-dialog/delete-warning-dialog.component";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {
  @Input() public task: Task = {} as Task;
  @Output() public onTaskDelete: EventEmitter<number> = new EventEmitter<number>();

  public isUpdating: boolean = false;

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
  }

  updateTask(task: Task){
    this.isUpdating = false;
    this.task = task;
  }

  deleteTask(): void{
    const dialogRef = this.dialog.open(DeleteWarningDialogComponent, {
      width: '400px'
    });

    dialogRef
      .afterClosed()
      .subscribe(result => {
        if(result == true) this.onTaskDelete.emit(this.task.id);
      });
  }
}
