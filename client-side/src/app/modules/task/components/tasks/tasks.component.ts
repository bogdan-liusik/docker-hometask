import {Component, OnInit, ViewChildren} from '@angular/core';
import {Task} from "../../models/task";
import {Subject} from "rxjs";
import {SnackBarService} from "../../../../services/snack-bar.service";
import { map, takeUntil } from "rxjs/operators";
import {TasksService} from "../../services/tasks-service";
import {TaskComponent} from "../task/task.component";


@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit {
  public tasks: Task[] = [];
  public isLoading: boolean = false;
  public creatingNewTask: boolean = false;

  private unsubscribe$ = new Subject<void>();

  @ViewChildren(TaskComponent) public innerComponents: TaskComponent[] = [];

  constructor(private taskService: TasksService,
              private snackBarService: SnackBarService) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.loadTasks();
  }

  addNewTask(task: Task): void{
    this.tasks.unshift(task);
    this.creatingNewTask = false;
  }

  private loadTasks(): void {
    this.taskService
      .getAllTasks()
      .pipe(
        takeUntil(this.unsubscribe$),
        map(tasks => tasks.sort((task1, task2) => task2.id - task1.id))
      )
      .subscribe(
        tasks => {
        this.tasks = tasks;
        this.isLoading = false;
      }, error => {
        this.isLoading = false;
        this.snackBarService.showErrorMessage('cant load data from server :(')
      });
  }

  deleteTask(id: number): void {
    this.isLoading = true;

    this.taskService
      .deleteTask(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        response => {
          this.isLoading = false;
          this.tasks = this.tasks.filter(task => task.id !== id);
          this.snackBarService.showSuccessMessage('Task deleted!');
        },
        error => {
          this.isLoading = false;
          this.snackBarService.showErrorMessage(error);
        }
      );
  }
}
