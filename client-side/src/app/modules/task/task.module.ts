import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TasksComponent } from './components/tasks/tasks.component'
import {MaterialComponentsModule} from "../../common/material-components.module";
import { TaskComponent } from './components/task/task.component';
import { UpdateTaskComponent } from './components/update-task/update-task.component';
import { CreateTaskComponent } from './components/create-task/create-task.component';
import {ReactiveFormsModule} from "@angular/forms";
import {TaskStateDirective} from "./directives/task-state.directive";
import {ProjectModule} from "../project/project.module";

@NgModule({
  declarations: [
    TasksComponent,
    TaskComponent,
    UpdateTaskComponent,
    CreateTaskComponent,
    TaskStateDirective,
  ],
  imports: [
    CommonModule,
    MaterialComponentsModule,
    ReactiveFormsModule,
    ProjectModule
  ]
})
export class TaskModule { }
