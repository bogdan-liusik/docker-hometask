import { Injectable } from "@angular/core";
import { HttpInternalService } from "../../../services/http-internal.service";
import { Observable } from "rxjs";
import { HttpResponse } from "@angular/common/http";
import {Task} from "../models/task";
import {UpdateTask} from "../models/update-task";
import {NewTask} from "../models/new-task";

@Injectable({ providedIn: 'root' })
export class TasksService {
  public routePrefix = '/api/tasks';

  constructor(private httpService: HttpInternalService) { }

  public getAllTasks(): Observable<Task[]> {
    return this.httpService.getRequest<Task[]>(`${this.routePrefix}`);
  }

  public createTask(newTask: NewTask): Observable<HttpResponse<Task>> {
    return this.httpService.postFullRequest<Task>(`${this.routePrefix}`, newTask);
  }

  public updateTask(updateTask: UpdateTask): Observable<HttpResponse<Task>> {
    return this.httpService.putFullRequest(`${this.routePrefix}`, updateTask);
  }

  public deleteTask(id: number) {
    return this.httpService.deleteFullRequest(`${this.routePrefix}/${id}`);
  }
}
