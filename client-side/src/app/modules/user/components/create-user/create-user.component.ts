import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Subject} from "rxjs";
import {SnackBarService} from "../../../../services/snack-bar.service";
import {UsersService} from "../../services/users-service";
import {takeUntil} from "rxjs/operators";
import {User} from "../../models/user";
import {NewUser} from "../../models/new-user";
import {userConstants} from "../../models/userConstants";

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {
  public isLoading: boolean = false;
  public formGroup: FormGroup = {} as FormGroup;
  public constants = userConstants;

  @Output() onCancelCreating = new EventEmitter<void>();
  @Output() onUserCreated = new EventEmitter<User>()

  private unsubscribe$ = new Subject<void>();

  constructor(private usersService: UsersService,
              private snackBarService: SnackBarService) { }

  ngOnInit(): void {
    this.formGroup = new FormGroup({
      firstName: new FormControl(
        '',
        [
          Validators.required,
          Validators.maxLength(this.constants.MAX_NAME_LENGTH),
          Validators.minLength(this.constants.MIN_NAME_LENGTH)]),

      lastName: new FormControl(
        '',
        [
          Validators.required,
          Validators.maxLength(this.constants.MAX_NAME_LENGTH),
          Validators.minLength(this.constants.MIN_NAME_LENGTH)]),

      email: new FormControl(
        '',
        [
          Validators.required,
          Validators.maxLength(this.constants.MAX_EMAIL_LENGTH),
          Validators.minLength(this.constants.MIN_EMAIL_LENGTH),
          Validators.email
        ]),

      teamId: new FormControl(
        '',
        [
          Validators.required,
          Validators.min(1)]),

      birthDay: new FormControl(
        '',
        [Validators.required]),

      createdAt: new FormControl(new Date()),
    });
  }

  onSubmit(): void{
    this.isLoading = true;
    const newUser: NewUser = <NewUser>this.formGroup.value;

    this.usersService
      .createUser(newUser)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        response => {
          const createdUser: User = <User>response.body;
          this.isLoading = false;
          this.onUserCreated.emit(createdUser);
          this.snackBarService.showSuccessMessage('Successfully created!');
        },
        error => {
          this.isLoading = false;
          this.snackBarService.showErrorMessage(error.error.error);
        }
      );
  }
}
