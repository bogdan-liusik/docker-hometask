import {Component, OnInit, ViewChildren} from '@angular/core';
import {SnackBarService} from "../../../../services/snack-bar.service";
import {UsersService} from "../../services/users-service";
import {map, takeUntil} from "rxjs/operators";
import {Subject} from "rxjs";
import {UserComponent} from "../user/user.component";
import {User} from "../../models/user";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  public users: User[] = [];
  public isLoading: boolean = false;
  public creatingNewUser: boolean = false;

  private unsubscribe$ = new Subject<void>();

  @ViewChildren(UserComponent) public innerComponents: UserComponent[] = [];

  constructor(private usersService: UsersService,
              private snackBarService: SnackBarService) { }

  ngOnInit(): void {
    this.loadUsers();
  }

  public addNewUser(user: User) {
    this.creatingNewUser = false;
    this.users.unshift(user);
  }

  public deleteUser(id: number){
    this.isLoading = true;

    this.usersService
      .deleteUser(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        response => {
          this.isLoading = false;
          this.users = this.users.filter(user => user.id !== id);
          this.snackBarService.showSuccessMessage('User deleted!');
        },
        error => {
          console.log(error)
          this.isLoading = false;
          this.snackBarService.showErrorMessage(error);
        }
      );
  }

  private loadUsers(): void {
    this.isLoading = true;

    this.usersService
      .getAllUsers()
      .pipe(
        takeUntil(this.unsubscribe$),
        map(users => users.sort((user1, user2) => user2.id - user1.id))
      )
      .subscribe(
        users => {
          this.users = users;
          this.isLoading = false;
        }, error => {
          this.isLoading = false;
          this.snackBarService.showErrorMessage('cant load data from server :(')
        }
      );
  }
}
