import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {DeleteWarningDialogComponent} from "../../../../common/delete-warning-dialog/delete-warning-dialog.component";
import {User} from "../../models/user";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  @Input() public user: User = {} as User;
  @Output() public onUserDelete = new EventEmitter<number>();

  public isUpdating: boolean = false;

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {

  }

  public updateUser(user: User): void {
    this.isUpdating = false;
    this.user = user;
  }

  public deleteUser(): void {
    const dialogRef = this.dialog.open(DeleteWarningDialogComponent, {
      width: '400px'
    });

    dialogRef
      .afterClosed()
      .subscribe(result => {
        if(result == true) this.onUserDelete.emit(this.user.id);
      });
  }
}
