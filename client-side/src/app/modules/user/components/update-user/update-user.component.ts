import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Subject} from "rxjs";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {SnackBarService} from "../../../../services/snack-bar.service";
import {UsersService} from "../../services/users-service";
import {takeUntil} from "rxjs/operators";
import {User} from "../../models/user";
import {UpdateUser} from "../../models/update-user";
import {userConstants} from "../../models/userConstants";

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.scss']
})
export class UpdateUserComponent implements OnInit {
  @Input() public user: User = {} as User;
  @Output() onCancelUpdate = new EventEmitter<void>();
  @Output() onUserUpdated = new EventEmitter<User>();

  public isLoading: boolean = false;
  private unsubscribe$ = new Subject<void>();
  public formGroup: FormGroup = {} as FormGroup;
  public constants = userConstants;

  constructor(private usersService: UsersService,
              private snackBarService: SnackBarService) { }

  ngOnInit(): void {
    this.formGroup = new FormGroup({
      id: new FormControl(this.user.id),

      firstName: new FormControl(
        this.user.firstName,
        [
          Validators.required,
          Validators.maxLength(this.constants.MAX_NAME_LENGTH),
          Validators.minLength(this.constants.MIN_NAME_LENGTH)]),

      lastName: new FormControl(
        this.user.lastName,
        [
          Validators.required,
          Validators.maxLength(this.constants.MAX_NAME_LENGTH),
          Validators.minLength(this.constants.MIN_NAME_LENGTH)]),

      email: new FormControl(
        this.user.email,
        [
          Validators.required,
          Validators.maxLength(this.constants.MAX_EMAIL_LENGTH),
          Validators.minLength(this.constants.MIN_EMAIL_LENGTH),
          Validators.email
        ]),
    });
  }

  public onSubmit(): void{
    this.isLoading = true;

    const updateUser: UpdateUser= <UpdateUser>this.formGroup.value;

    this.usersService
      .updateUser(updateUser)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        response => {
          this.isLoading = false;
          this.user = <User>response.body;
          this.onUserUpdated.emit(this.user);
          this.snackBarService.showSuccessMessage('Successfully updated!');
        },
        error => {
          this.isLoading = false;
          this.snackBarService.showErrorMessage(error.error.error);
        });
  }
}
