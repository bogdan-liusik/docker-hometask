export interface User {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  teamId: string;
  birthDay: Date;
  createdAt: Date;
}
