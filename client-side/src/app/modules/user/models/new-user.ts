export interface NewUser {
  firstName: string;
  lastName: string;
  email: string;
  teamId: string;
  birthDay: Date;
  createdAt: Date;
}
