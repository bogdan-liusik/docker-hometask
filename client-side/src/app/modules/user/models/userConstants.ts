export const userConstants = {
  MAX_NAME_LENGTH: 80,
  MIN_NAME_LENGTH: 3,
  MAX_EMAIL_LENGTH: 40,
  MIN_EMAIL_LENGTH: 4,
} as const;

