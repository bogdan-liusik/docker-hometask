import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from "rxjs";
import {UsersComponent} from "../components/users/users.component";

@Injectable({
  providedIn: 'root'
})
export class UsersGuard implements CanDeactivate<UsersComponent>{
  canDeactivate(
    component: UsersComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot)
    : Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const updatingTask: boolean =  component.innerComponents
      .some(component => component.isUpdating);

    if(component.creatingNewUser || updatingTask){
      return window.confirm('You have unsaved changes! Leave?')
    }

    return true;
  }
}
