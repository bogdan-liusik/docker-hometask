import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './components/users/users.component';
import { UserComponent } from './components/user/user.component';
import {MaterialComponentsModule} from "../../common/material-components.module";
import { UpdateUserComponent } from './components/update-user/update-user.component';
import {ReactiveFormsModule} from "@angular/forms";
import { CreateUserComponent } from './components/create-user/create-user.component';
import {ProjectModule} from "../project/project.module";



@NgModule({
  declarations: [
    UsersComponent,
    UserComponent,
    UpdateUserComponent,
    CreateUserComponent,
  ],
    imports: [
        CommonModule,
        MaterialComponentsModule,
        ReactiveFormsModule,
        ProjectModule,
    ]
})
export class UserModule { }
