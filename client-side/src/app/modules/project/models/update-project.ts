export interface UpdateProject {
  id: number;
  name: string;
  description: string;
  teamId: number;
  deadline: Date;
}
