export interface NewProject {
  name: string;
  description: string;
  authorId: number;
  teamId: number;
  createdAt: Date;
  deadline: Date;
}
