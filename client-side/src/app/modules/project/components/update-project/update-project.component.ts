import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ProjectsService} from "../../services/projects-service";
import {SnackBarService} from "../../../../services/snack-bar.service";
import {Subject} from "rxjs";
import {takeUntil} from "rxjs/operators";
import {Project} from "../../models/project";
import {UpdateProject} from "../../models/update-project";
import {projectConstants} from "../../models/projectConstants";

@Component({
  selector: 'app-update-project',
  templateUrl: './update-project.component.html',
  styleUrls: ['./update-project.component.scss']
})
export class UpdateProjectComponent implements OnInit{
  @Input() public project: Project = {} as Project;
  @Output() onCancelUpdate = new EventEmitter<void>();
  @Output() onProjectUpdated = new EventEmitter<Project>();

  public isLoading: boolean = false;
  private unsubscribe$ = new Subject<void>();
  public formGroup: FormGroup = {} as FormGroup;
  public constants = projectConstants;

  constructor(private projectService: ProjectsService,
              private snackBarService: SnackBarService) { }

  ngOnInit(): void {
    this.formGroup = new FormGroup({
      id: new FormControl(
        this.project.id
      ),
      name: new FormControl(
        this.project.name,
        [
          Validators.required,
          Validators.minLength(this.constants.MIN_PROJECT_NAME_LENGTH),
          Validators.maxLength(this.constants.MAX_PROJECT_NAME_LENGTH)]),

      description: new FormControl(
        this.project.description,
        [
          Validators.required,
          Validators.minLength(this.constants.MIN_PROJECT_DESCRIPTION_LENGTH),
          Validators.maxLength(this.constants.MAX_PROJECT_DESCRIPTION_LENGTH)]),

      teamId: new FormControl(
        this.project.teamId,
        [Validators.min(1)]),

      deadline: new FormControl(
        this.project.deadline,
        [Validators.required]),
    });
  }

  public onSubmit(): void{
    this.isLoading = true;

    const updateProject: UpdateProject = <UpdateProject>this.formGroup.value;

    this.projectService
      .updateProject(updateProject)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        response => {
          this.isLoading = false;
          this.project = <Project>response.body;
          this.onProjectUpdated.emit(this.project);
          this.snackBarService.showSuccessMessage('Successfully updated!');
        },
        error => {
          this.isLoading = false;
          this.snackBarService.showErrorMessage(error.error.error);
        });
  }
}
