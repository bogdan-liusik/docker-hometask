import {Component, OnInit, ViewChildren} from '@angular/core';
import {ProjectsService} from "../../services/projects-service";
import {Subject} from "rxjs";
import {map, takeUntil} from "rxjs/operators";
import {SnackBarService} from "../../../../services/snack-bar.service";
import {ProjectComponent} from "../project/project.component";
import {Project} from "../../models/project";

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {
  public projects: Project[] = [];
  public isLoading: boolean = false;
  public creatingNewProject: boolean = false;

  private unsubscribe$ = new Subject<void>();

  @ViewChildren(ProjectComponent) public innerComponents: ProjectComponent[] = [];

  constructor(private projectService: ProjectsService,
              private snackBarService: SnackBarService) { }

  ngOnInit(): void {
    this.loadProjects();
  }

  public addNewProject(project: Project): void {
    this.projects.unshift(project);
    this.creatingNewProject = false;
  }

  public deleteProject(id: number): void {
    this.isLoading = true;

    this.projectService
      .deleteProject(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        response => {
          this.isLoading = false;
          this.projects = this.projects.filter(project => project.id !== id);
          this.snackBarService.showSuccessMessage('Project deleted!');
        },
        error => {
          this.isLoading = false;
          this.snackBarService.showErrorMessage(error);
        }
      );
  }

  private loadProjects(): void {
    this.isLoading = true;

    this.projectService
      .getAllProjects()
      .pipe(
        takeUntil(this.unsubscribe$),
        map(projects => projects.sort((project1, project2) => project2.id - project1.id))
      )
      .subscribe(projects => {
        this.projects = projects;
        this.isLoading = false;
      }, error => {
        this.isLoading = false;
        this.snackBarService.showErrorMessage('cant load data from server :(')
      });
  }
}
