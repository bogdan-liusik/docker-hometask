import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Subject} from "rxjs";
import {ProjectsService} from "../../services/projects-service";
import {SnackBarService} from "../../../../services/snack-bar.service";
import {takeUntil} from "rxjs/operators";
import {Project} from "../../models/project";
import {NewProject} from "../../models/new-project";
import {projectConstants} from "../../models/projectConstants";

@Component({
  selector: 'app-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.scss'],
})
export class CreateProjectComponent implements OnInit {
  public isLoading: boolean = false;
  public formGroup: FormGroup = {} as FormGroup;
  public constants = projectConstants;

  @Output() onCancelCreating = new EventEmitter<void>();
  @Output() onProjectCreated = new EventEmitter<Project>()

  private unsubscribe$ = new Subject<void>();

  constructor(private projectService: ProjectsService,
              private snackBarService: SnackBarService) { }

  ngOnInit(): void {
    this.formGroup = new FormGroup({
      name: new FormControl(
        '',
        [
          Validators.required,
          Validators.minLength(this.constants.MIN_PROJECT_NAME_LENGTH),
          Validators.maxLength(this.constants.MAX_PROJECT_NAME_LENGTH)]),

      description: new FormControl(
        '',
        [
          Validators.required,
          Validators.minLength(this.constants.MIN_PROJECT_DESCRIPTION_LENGTH),
          Validators.maxLength(this.constants.MAX_PROJECT_DESCRIPTION_LENGTH)]),

      deadline: new FormControl(
        '',
        [Validators.required]),

      authorId: new FormControl(
        '',
        [
          Validators.required,
          Validators.min(1)]
        ),

      teamId: new FormControl(
        '',
        [
          Validators.required,
          Validators.min(1)]
        ),

      createdAt: new FormControl(new Date()),
    });
  }

  onSubmit(): void{
    this.isLoading = true;
    const newProject: NewProject = <NewProject>this.formGroup.value;

    this.projectService
      .createProject(newProject)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        response => {
          const createdProject: Project = <Project>response.body;
          this.isLoading = false;
          this.onProjectCreated.emit(createdProject);
          this.snackBarService.showSuccessMessage('Successfully created!');
        },
        error => {

          this.isLoading = false;
          this.snackBarService.showErrorMessage(error.error.error);
        });
  }
}
