import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {DeleteWarningDialogComponent} from "../../../../common/delete-warning-dialog/delete-warning-dialog.component";
import {Project} from "../../models/project";

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements OnInit {
  @Input() public project: Project = {} as Project;
  @Output() public onProjectDelete = new EventEmitter<number>();

  public isUpdating: boolean = false;

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void { }

  updateProject(project: Project): void{
    this.isUpdating = false;
    this.project = project;
  }

  deleteProject(): void{
    const dialogRef = this.dialog.open(DeleteWarningDialogComponent, {
      width: '400px'
    });

    dialogRef
      .afterClosed()
      .subscribe(result => {
        if(result == true) this.onProjectDelete.emit(this.project.id);
    });
  }
}
