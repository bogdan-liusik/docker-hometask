import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import {ProjectsComponent} from "../components/projects/projects.component";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ProjectsGuard implements CanDeactivate<ProjectsComponent>{
  canDeactivate(
    component: ProjectsComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot)
    : Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const updatingProject: boolean =  component.innerComponents
      .some(component => component.isUpdating);

    if(component.creatingNewProject || updatingProject){
      return window.confirm('You have unsaved changes! Leave?')
    }

    return true;
  }
}
