import {Injectable} from "@angular/core";
import {HttpInternalService} from "../../../services/http-internal.service";
import {Observable} from "rxjs";
import {HttpResponse} from "@angular/common/http";
import {Project} from "../models/project";
import {UpdateProject} from "../models/update-project";
import {NewProject} from "../models/new-project";

@Injectable({ providedIn: 'root' })
export class ProjectsService {
  public readonly routePrefix = '/api/projects';

  constructor(private httpService: HttpInternalService) { }

  public getAllProjects(): Observable<Project[]> {
    return this.httpService.getRequest<Project[]>(`${this.routePrefix}`);
  }

  public createProject(newProject: NewProject): Observable<HttpResponse<Project>> {
    return this.httpService.postFullRequest<Project>(`${this.routePrefix}`, newProject);
  }

  public updateProject(updateProject: UpdateProject): Observable<HttpResponse<Project>> {
    return this.httpService.putFullRequest(`${this.routePrefix}`, updateProject);
  }

  public deleteProject(id: number) {
    return this.httpService.deleteFullRequest(`${this.routePrefix}/${id}`);
  }
}

