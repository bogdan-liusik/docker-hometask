import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ukrainianDate'
})
export class UkrainianDatePipe implements PipeTransform {
  readonly monthes: string[] = [];

  constructor() {
    this.monthes = [
      'січня',
      'лютого',
      'березня',
      'квітня',
      'травня',
      'червня',
      'липня',
      'серпня',
      'вересня',
      'жовтня',
      'листопада',
      'грудня'
    ];
  }

  transform(value: Date | string): string {
    const date = new Date(value);
    return `${date.getDate()} ${this.monthes[date.getMonth()]} ${date.getFullYear()}`;
  }
}
