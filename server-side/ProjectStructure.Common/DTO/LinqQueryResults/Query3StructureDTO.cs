﻿namespace ProjectStructure.Common.DTO.LinqQueryResults
{
    public class Query3StructureDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}