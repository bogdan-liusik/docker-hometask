﻿using AutoMapper;
using ProjectStructure.Common.DTO.Task;
using ProjectStructure.DAL.Entities;

namespace ProjectStructure.BLL.MappingProfiles
{
    public class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<Task, TaskDTO>();
            CreateMap<TaskDTO, Task>();

            CreateMap<Task, TaskCreateDTO>();
            CreateMap<TaskCreateDTO, Task>();
            
            CreateMap<Task, TaskUpdateDTO>();
            CreateMap<TaskUpdateDTO, Task>();
        }
    }
}