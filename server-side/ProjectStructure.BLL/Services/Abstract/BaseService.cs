﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Entities.Abstract;

namespace ProjectStructure.BLL.Services.Abstract
{
    public abstract class BaseService
    {
        private protected readonly IMapper _mapper;
        private protected readonly ProjectsContext _context;

        public BaseService(ProjectsContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        protected async Task<ICollection<TEntity>> GetAllEntitiesAsync<TEntity>() where TEntity: BaseEntity
        {
            return await _context.Set<TEntity>().ToListAsync();
        }

        protected async Task<TEntity> GetEntityByIdAsync<TEntity>(int entityId) where TEntity: BaseEntity
        {
            var entity = await _context.Set<TEntity>().SingleOrDefaultAsync(e => e.Id == entityId);

            if (entity == null)
            {
                throw new NotFoundException(typeof(TEntity).ToString(), entityId);
            }
            
            return entity;
        }
    }
}