﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.Services.Abstract;
using ProjectStructure.Common.DTO.Task;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Entities.Enums;
using CustomTask = ProjectStructure.DAL.Entities.Task;

namespace ProjectStructure.BLL.Services
{
    public class TaskService : BaseService, ITaskService
    {
        public TaskService(ProjectsContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<ICollection<TaskDTO>> GetAllTasks()
        {
            var tasks = await GetAllEntitiesAsync<CustomTask>();
            return await Task.WhenAll(tasks
                .Select(async task => await Task.Run(() => _mapper.Map<TaskDTO>(task)))
                .ToList());
        }

        public async Task<TaskDTO> GetTaskById(int id)
        {
            var taskEntity = await GetEntityByIdAsync<CustomTask>(id);
            return _mapper.Map<TaskDTO>(taskEntity);
        }

        public async Task<TaskDTO> CreateTask(TaskCreateDTO taskCreateDto)
        {
            if (!await _context.Projects.AnyAsync(p => p.Id == taskCreateDto.ProjectId))
            {
                throw new NotFoundException($"Project ID {taskCreateDto.ProjectId}. No projects with such id.");
            }
            
            if (!await _context.Users.AnyAsync(u => u.Id == taskCreateDto.PerformerId))
            {
                throw new NotFoundException($"Performer ID {taskCreateDto.PerformerId}. No users with such id.");
            }
            
            var taskEntity = _mapper.Map<CustomTask>(taskCreateDto);
            if (taskEntity.State == TaskState.Done)
            {
                taskEntity.FinishedAt = DateTime.Now;
            }
            else
            {
                taskEntity.FinishedAt = null;
            }
            var createdTask = await _context.Tasks.AddAsync(taskEntity);
            await _context.SaveChangesAsync();
            return _mapper.Map<TaskDTO>(createdTask.Entity);
        }

        public async Task<TaskDTO> UpdateTask(TaskUpdateDTO taskUpdateDto)
        {
            var taskEntity = await GetEntityByIdAsync<CustomTask>(taskUpdateDto.Id);

            taskEntity.Name = taskUpdateDto.Name;
            taskEntity.Description = taskUpdateDto.Description;
            taskEntity.State = taskUpdateDto.State;

            if (taskEntity.State == TaskState.Done)
            {
                taskEntity.FinishedAt = DateTime.Now;
            }

            await _context.SaveChangesAsync();
            return _mapper.Map<TaskDTO>(taskEntity);
        }
        
        public async Task DeleteTask(int id)
        {
            var taskEntity = await GetEntityByIdAsync<CustomTask>(id);
            _context.Tasks.Remove(taskEntity);
            await _context.SaveChangesAsync();
        }
    }
}