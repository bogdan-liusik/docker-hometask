﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.Services.Abstract;
using ProjectStructure.Common.DTO.Team;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Entities;
using Task = System.Threading.Tasks.Task;

namespace ProjectStructure.BLL.Services
{
    public class TeamService : BaseService, ITeamService
    {
        public TeamService(ProjectsContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<ICollection<TeamDTO>> GetAllTeams()
        {
            var teams = await GetAllEntitiesAsync<Team>();
            return await Task.WhenAll(teams
                .Select(async team => await Task.Run(() => _mapper.Map<TeamDTO>(team)))
                .ToList());
        }

        public async Task<TeamDTO> GetTeamById(int id)
        {
            var teamEntity = await GetEntityByIdAsync<Team>(id);
            return _mapper.Map<TeamDTO>(teamEntity);
        }

        public async Task<TeamDTO> CreateTeam(TeamCreateDTO teamCreateDto)
        {
            if (teamCreateDto.Name == null)
            {
                throw new InvalidBodyException(typeof(TeamCreateDTO).ToString());
            }
            
            var teamEntity = _mapper.Map<Team>(teamCreateDto);
            var createdTeam = await _context.Teams.AddAsync(teamEntity);
            
            await _context.SaveChangesAsync();
            return _mapper.Map<TeamDTO>(createdTeam.Entity);
        }

        public async Task<TeamDTO> UpdateTeam(TeamUpdateDTO teamUpdateDto)
        {
            var teamEntity = await GetEntityByIdAsync<Team>(teamUpdateDto.Id);

            teamEntity.Name = teamUpdateDto.Name;
            
            await _context.SaveChangesAsync();
            return _mapper.Map<TeamDTO>(teamEntity);
        }

        public async Task DeleteTeam(int id)
        {
            var entityTeam = await GetEntityByIdAsync<Team>(id);
            _context.Teams.Remove(entityTeam);
            await _context.SaveChangesAsync();
        }
    }
}