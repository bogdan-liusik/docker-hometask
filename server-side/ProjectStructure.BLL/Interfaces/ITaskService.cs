﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectStructure.Common.DTO.Task;

namespace ProjectStructure.BLL.Interfaces
{
    public interface ITaskService
    {
        Task<ICollection<TaskDTO>> GetAllTasks();
        Task<TaskDTO> GetTaskById(int projectId);
        Task<TaskDTO> CreateTask(TaskCreateDTO project);
        Task<TaskDTO> UpdateTask(TaskUpdateDTO project);
        Task DeleteTask(int projectId);
    }
}