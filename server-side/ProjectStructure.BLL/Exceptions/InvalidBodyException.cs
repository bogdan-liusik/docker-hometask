﻿using System;

namespace ProjectStructure.BLL.Exceptions
{
    public class InvalidBodyException: Exception
    {
        public InvalidBodyException(string name) : base($"Entity {name} has invalid body.") { }
    }
}