FROM mcr.microsoft.com/dotnet/sdk:5.0.300 AS build-env
WORKDIR /app

COPY *.sln .
COPY ProjectStructure.BLL/*.csproj ./ProjectStructure.BLL/
COPY ProjectStructure.BLL.Tests/*.csproj ./ProjectStructure.BLL.Tests/
COPY ProjectStructure.Common/*.csproj ./ProjectStructure.Common/
COPY ProjectStructure.DAL/*.csproj ./ProjectStructure.DAL/
COPY ProjectStructure.WebAPI/*.csproj ./ProjectStructure.WebAPI/
COPY ProjectStructure.WebAPI.IntegrationTests/*.csproj ./ProjectStructure.WebAPI.IntegrationTests/

COPY ProjectStructure.BLL ./ProjectStructure.BLL/
COPY ProjectStructure.BLL.Tests/ ./ProjectStructure.BLL.Tests/
COPY ProjectStructure.Common/ ./ProjectStructure.Common/
COPY ProjectStructure.DAL/ ./ProjectStructure.DAL/
COPY ProjectStructure.WebAPI/ ./ProjectStructure.WebAPI/
COPY ProjectStructure.WebAPI.IntegrationTests/ ./ ProjectStructure.WebAPI.IntegrationTests/

WORKDIR /app/ProjectStructure.WebAPI
RUN dotnet publish -c Release -o out

FROM mcr.microsoft.com/dotnet/aspnet:5.0.3
WORKDIR /app

COPY --from=build-env /app/ProjectStructure.WebAPI/out ./
ENTRYPOINT ["dotnet", "ProjectStructure.WebAPI.dll"]
