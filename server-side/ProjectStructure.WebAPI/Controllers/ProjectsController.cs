﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common.DTO.Project;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectService _projectService;

        public ProjectsController(IProjectService projectService)
        {
            _projectService = projectService;
        }
        
        [HttpGet]
        public async Task<ActionResult<ICollection<ProjectDTO>>> GetAllProjects()
        {
            return Ok(await _projectService.GetAllProjects());
        }
        
        [HttpGet("{id:int}")]
        public async Task<ActionResult<ProjectDTO>> GetProjectById(int id)
        {
            return Ok(await _projectService.GetProjectById(id));
        }
        
        [HttpPost]
        public async Task<ActionResult<ProjectDTO>> CreateProject([FromBody] ProjectCreateDTO projectCreateDto)
        {
            return Created("",await _projectService.CreateProject(projectCreateDto));
        }

        [HttpPut]
        public async Task<ActionResult<ProjectDTO>> UpdateProject([FromBody] ProjectUpdateDTO projectUpdateDto)
        {
            return Ok(await _projectService.UpdateProject(projectUpdateDto));
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> DeleteProject(int id)
        {
            await _projectService.DeleteProject(id);
            return NoContent();
        }
    }
}