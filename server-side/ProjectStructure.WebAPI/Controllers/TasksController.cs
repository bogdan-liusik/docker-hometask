﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common.DTO.Task;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITaskService _taskService;
        
        public TasksController(ITaskService taskService)
        {
            _taskService = taskService;
        }
        
        [HttpGet]
        public async Task<ActionResult<ICollection<TaskDTO>>> GetAllTasks()
        {
            return Ok(await _taskService.GetAllTasks());
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<TaskDTO>> GetTaskById(int id)
        {
            return Ok(await _taskService.GetTaskById(id));
        }

        [HttpPost]
        public async Task<ActionResult<TaskDTO>> CreateTask([FromBody] TaskCreateDTO taskCreateDto)
        {
            return Ok(await _taskService.CreateTask(taskCreateDto));
        }

        [HttpPut]
        public async Task<ActionResult<TaskDTO>> UpdateTask([FromBody] TaskUpdateDTO taskUpdateDto)
        {
            return Ok(await _taskService.UpdateTask(taskUpdateDto));
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> DeleteTask(int id)
        {
            await _taskService.DeleteTask(id);
            return NoContent();
        }
    }
}