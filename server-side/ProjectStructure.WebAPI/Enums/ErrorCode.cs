﻿namespace ProjectStructure.WebAPI.Enums
{
    public enum ErrorCode
    {
        BadRequest = 400,
        NotFound = 404,
        InternalServerError = 500
    }
}