﻿using Xunit;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ProjectStructure.Common.DTO.Task;
using ProjectStructure.Common.DTO.User;
using ProjectStructure.DAL.Entities.Enums;
using ProjectStructure.WebAPI.IntegrationTests.Factories;

namespace ProjectStructure.WebAPI.IntegrationTests.Tests
{
    public class UsersControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory>
    {
        private readonly HttpClient _httpClient;
        private readonly string _endpoint;
            
        public UsersControllerIntegrationTests(CustomWebApplicationFactory factory)
        {
            _httpClient = factory.CreateClient();
            _endpoint = "api/users";
        }


        [Fact]
        public async Task GetAllUnfinishedTasks_WhenAddOneFinishedAndOneUnfinishedTask_Than200OkAndUnfinishedTasksLength1()
        {
            var user = new UserCreateDTO()
            {
                FirstName = "Bogdan",
                LastName = "Liusik",
                Email = "bohdanliusik@gmail.com",
                BirthDay = DateTime.Parse("31/12/2002"),
                CreatedAt = DateTime.Now,
            };
            
            var createdUserResponse = await _httpClient.PostAsync(
                requestUri: _endpoint, 
                content: new StringContent(JsonConvert.SerializeObject(user), 
                    encoding: Encoding.UTF8, 
                    mediaType: "application/json"));
            
            var createdUser = JsonConvert.DeserializeObject<UserDTO>(await createdUserResponse.Content.ReadAsStringAsync());
            
            var task1 = new TaskCreateDTO() 
            { 
                Name = "TaskName1",
                Description = "TaskDescription1",
                PerformerId = createdUser.Id,
                ProjectId = 1,
                State = TaskState.InProgress,
                CreatedAt = DateTime.Now,
            };
            
            var task2 = new TaskCreateDTO() 
            { 
                Name = "TaskName2",
                Description = "TaskDescription2",
                PerformerId = createdUser.Id,
                ProjectId = 1,
                State = TaskState.Done,
                CreatedAt = DateTime.Now,
            };
            
            await _httpClient.PostAsync(
                    requestUri: "api/tasks", 
                    content: new StringContent(JsonConvert.SerializeObject(task1), 
                    encoding:Encoding.UTF8, 
                    mediaType: "application/json"));
            await _httpClient.PostAsync(
                    requestUri: "api/tasks", 
                    content: new StringContent(JsonConvert.SerializeObject(task2), 
                    encoding:Encoding.UTF8, 
                    mediaType: "application/json"));
            
            var response = await _httpClient.GetAsync($"{_endpoint}/unfinished_tasks/{createdUser.Id}");
            var result = JsonConvert.DeserializeObject<ICollection<TaskDTO>>(await response.Content.ReadAsStringAsync());
            
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Equal(1, result.Count);
        }
        
        [Fact]
        public async Task GetAllUnfinishedTasks_WhenUserDoesNotExist_Than404NotFound()
        {
            var user = new UserCreateDTO()
            {
                FirstName = "Bogdan",
                LastName = "Liusik",
                Email = "bohdanliusik@gmail.com",
                BirthDay = DateTime.Parse("31/12/2002"),
                CreatedAt = DateTime.Now,
            };
            
            var createdUserResponse = await _httpClient.PostAsync(
                    requestUri: _endpoint, 
                    content: new StringContent(JsonConvert.SerializeObject(user), 
                    encoding: Encoding.UTF8, 
                    mediaType: "application/json"));
            
            var createdUser = JsonConvert.DeserializeObject<UserDTO>(await createdUserResponse.Content.ReadAsStringAsync());
            
            await _httpClient.DeleteAsync($"{_endpoint}/{createdUser.Id}");
            await _httpClient.DeleteAsync($"{_endpoint}/{createdUser.Id}");

            var response = await _httpClient.GetAsync($"{_endpoint}/unfinished_tasks/{createdUser.Id}");
            
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task DeleteUser_WhenUserExists_Then204NoContent()
        {
            var user = new UserCreateDTO()
            {
                FirstName = "Bogdan",
                LastName = "Liusik",
                Email = "bohdanliusik@gmail.com",
                BirthDay = DateTime.Parse("31/12/2002"),
                CreatedAt = DateTime.Now,
            };
            
            var createdUserResponse = await _httpClient.PostAsync(
                    requestUri: _endpoint, 
                    content: new StringContent(JsonConvert.SerializeObject(user), 
                    encoding:Encoding.UTF8, 
                    mediaType: "application/json"));
            
            var createdUserContent = await createdUserResponse.Content.ReadAsStringAsync();
            var createdUser = JsonConvert.DeserializeObject<UserDTO>(createdUserContent);
            
            var deleteUserResponse = await _httpClient.DeleteAsync($"{_endpoint}/{createdUser.Id}");
            
            Assert.Equal(HttpStatusCode.NoContent, deleteUserResponse.StatusCode);
        }
        
        [Fact]
        public async Task DeleteUser_WhenUserDoesNotExist_Than404NotFound()
        {
            var user = new UserCreateDTO()
            {
                FirstName = "Bogdan",
                LastName = "Liusik",
                Email = "bohdanliusik@gmail.com",
                BirthDay = DateTime.Parse("31/12/2002"),
                CreatedAt = DateTime.Now,
            };
            
            var createdUserResponse = await _httpClient.PostAsync(
                    requestUri: _endpoint, 
                    content: new StringContent(JsonConvert.SerializeObject(user), 
                    encoding: Encoding.UTF8, 
                    mediaType: "application/json"));
            
            var createdUserContent = await createdUserResponse.Content.ReadAsStringAsync();
            var createdUser = JsonConvert.DeserializeObject<UserDTO>(createdUserContent);
            
            //delete first time.
            await _httpClient.DeleteAsync($"{_endpoint}/{createdUser.Id}");
            //delete second time a non-existent user.
            var deleteUserResponse = await _httpClient.DeleteAsync($"{_endpoint}/{createdUser.Id}");
            
            Assert.Equal(HttpStatusCode.NotFound, deleteUserResponse.StatusCode);
        }
    }
}