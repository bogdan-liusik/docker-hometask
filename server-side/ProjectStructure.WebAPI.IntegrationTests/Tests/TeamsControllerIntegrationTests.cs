﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ProjectStructure.Common.DTO.Team;
using ProjectStructure.WebAPI.IntegrationTests.Factories;
using Xunit;

namespace ProjectStructure.WebAPI.IntegrationTests.Tests
{
    public class TeamsControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory>
    {
        private readonly HttpClient _httpClient;
        private readonly string _endpoint;

        public TeamsControllerIntegrationTests(CustomWebApplicationFactory factory)
        {
            _httpClient = factory.CreateClient();
            _endpoint = "api/teams";
        }

        [Fact]
        public async Task CreateTeam_WhenTeamModelValid_ThenStatusCode201Created()
        {
            var team = new TeamCreateDTO()
            {
                Name = "SuperTeam",
                CreatedAt = DateTime.Now
            };
            
            var response = await _httpClient.PostAsync(
                    requestUri: _endpoint, 
                    content: new StringContent(JsonConvert.SerializeObject(team), 
                    encoding: Encoding.UTF8, 
                    mediaType: "application/json"));
            
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }
        
        [Fact]
        public async Task CreateTeam_WhenTeamModelInValid_ThenStatusCode400BadRequest()
        {
            var team = new TeamCreateDTO();

            var response = await _httpClient.PostAsync(
                    requestUri: _endpoint, 
                    content: new StringContent(JsonConvert.SerializeObject(team), 
                    encoding: Encoding.UTF8, 
                    mediaType: "application/json"));
            
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}