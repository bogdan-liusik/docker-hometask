﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ProjectStructure.DAL.Entities.Abstract;

namespace ProjectStructure.DAL.Entities
{
    public class Team : BaseEntity
    {
        public string Name { get; set; }
        public ICollection<User> Members { get; set; }
        public ICollection<Project> Projects { get; set; }
    }
}