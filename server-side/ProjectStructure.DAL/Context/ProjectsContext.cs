﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Entities.Abstract;

namespace ProjectStructure.DAL.Context
{
    public class ProjectsContext : DbContext
    {
        public DbSet<Project> Projects { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Task> Tasks { get; set; }

        public ProjectsContext(DbContextOptions<ProjectsContext> options) : base(options)
        {
            Database.EnsureDeleted();
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ConfigureRelations();
            modelBuilder.ConfigureModels();
            modelBuilder.Seed();
        }
    }
    
    public static class DbContextExtensions
    {
        public static TEntity CustomUpdate<TEntity>(this DbContext context, TEntity entity) where TEntity : BaseEntity 
        {
            var local = context.Set<TEntity>().Local.FirstOrDefault(entry => entry.Id.Equals(entity.Id));
            if (local != null)
            {
                context.Entry(local).State = EntityState.Detached;
            }
            var result = context.Entry(entity);
            result.State = EntityState.Modified;
            return result.Entity;
        }
    }
}