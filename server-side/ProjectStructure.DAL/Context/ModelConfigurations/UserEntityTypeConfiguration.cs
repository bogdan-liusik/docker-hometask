﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProjectStructure.DAL.Entities;

namespace ProjectStructure.DAL.Context.ModelConfigurations
{
    public class UserEntityTypeConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder
                .Ignore(u => u.FullName);
            
            builder
                .Property(u => u.Email)
                .IsRequired();

            builder
                .Property(u => u.FirstName)
                .HasMaxLength(30)
                .IsRequired();

            builder
                .Property(u => u.LastName)
                .HasMaxLength(30)
                .IsRequired();
            
        }
    }
}