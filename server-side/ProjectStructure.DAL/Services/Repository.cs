﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Entities.Abstract;
using ProjectStructure.DAL.Interfaces;

namespace ProjectStructure.DAL.Services
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity: BaseEntity
    {
        private readonly ProjectsContext _context;
        private readonly DbSet<TEntity> _entities;
        
        public Repository(ProjectsContext context)
        {
            _context = context;
            _entities = _context.Set<TEntity>();
        }

        public List<TEntity> GetAll(Expression<Func<TEntity, bool>> filter = null)
        {
            return filter == null ? _entities.ToList() : _entities.Where(filter).ToList();
        }

        public TEntity Get(int id)
        {
            return _entities.SingleOrDefault(e => e.Id == id);
        }

        public TEntity Create(TEntity entityToCreate)
        {
            return _entities.Add(entityToCreate).Entity;
        }
        
        public TEntity Update(TEntity entityToUpdate)
        {
            return _context.CustomUpdate(entityToUpdate);
        }
        
        public void Delete(int id)
        {
            var entity = _entities.SingleOrDefault(e => e.Id == id);
            _entities.Remove(entity);
        }
    }
}